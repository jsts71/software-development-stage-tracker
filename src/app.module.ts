import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProjectModule } from './project/project.module';
import { PlanningModule } from './planning/planning.module';
import { AnalysisModule } from './analysis/analysis.module';
import { RequirementModule } from './requirement/requirement.module';
import { DesignModule } from './design/design.module';
import { DevelopmentModule } from './development/development.module';
import { TestingModule } from './testing/testing.module';
import { DeploymentModule } from './deployment/deployment.module';
import { MaintenanceModule } from './maintenance/maintenance.module';

@Module({
  imports: [ProjectModule, PlanningModule, AnalysisModule, RequirementModule, DesignModule, DevelopmentModule, TestingModule, DeploymentModule, MaintenanceModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
