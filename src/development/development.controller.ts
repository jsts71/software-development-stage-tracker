import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { DevelopmentService } from './development.service';
import { CreateDevelopmentDto } from './dto/create-development.dto';
import { UpdateDevelopmentDto } from './dto/update-development.dto';

@Controller('development')
export class DevelopmentController {
  constructor(private readonly developmentService: DevelopmentService) {}

  @Post()
  create(@Body() createDevelopmentDto: CreateDevelopmentDto) {
    return this.developmentService.create(createDevelopmentDto);
  }

  @Get()
  findAll() {
    return this.developmentService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.developmentService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDevelopmentDto: UpdateDevelopmentDto) {
    return this.developmentService.update(+id, updateDevelopmentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.developmentService.remove(+id);
  }
}
